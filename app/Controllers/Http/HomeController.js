//app/Controllers/Http/HomeController.js
'use strict'

// Recuerde referenciar el controlador YoutubeController en la parte de arriba
const YoutubeController = use('App/Controllers/Http/YoutubeController')
const Youtube = new YoutubeController()

class HomeController {

  async index({ view }) {

    return view.render('index', {link: ''})

  }

  async search({ view, request, response, session }) {

    const link = request.input('link')

    var video = null

    await Youtube.getData( link )
      // Indica que el video existe
      .then( ( info ) => {

        const detail = info.player_response.videoDetails;
        video = {
          id: detail.videoId,
          link: link,
          title: detail.title,
          thumb: detail.thumbnail.thumbnails[detail.thumbnail.thumbnails.length - 1].url
        }

      })
      // Indica que el video no existe o hubo un error
      .catch( ( err ) => {

        session.flash({
          notification_class: 'alert-danger',
          notification_icon: 'fa-times',
          notification_message: 'ERROR: Video not found. Try again.'
        })

      });

    if(video == null)
      return response.redirect('/')
    else
      return view.render('index', {link: video.link, video: video})

  }

  async download({ view, request, response }) {

    const link = request.input('link')
    const title = request.input('title')

    return Youtube.download( link, title, response )

  }

}

module.exports = HomeController